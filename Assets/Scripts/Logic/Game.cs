﻿using System;

public class Game {

	public GameState state;
	public readonly References references;
	public readonly GameSettings settings;
	public long Score { get; set; }

	public Game(References references, GameSettings settings) {
		this.references = references;
		this.settings = settings;

		this.state = new IntroState(this);

		settings.backgroundMusic.Play(references.backgroundAudioSource);
	}

	public void Update() {
		state.Update();
	}
}
