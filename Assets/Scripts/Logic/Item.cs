﻿
using System;

public class Item {

	public readonly Guid guid;
	public readonly ItemData data;

	public readonly ItemReferences references;

	public Item(ItemReferences references, ItemData data) {
		this.references = references;
		this.data = data;
		this.guid = Guid.NewGuid();

		references.spriteRenderer.sprite = data.sprite;
		references.clickReference.DoAction += Destroy;
	}

	public void Destroy() {
		references.spriteRenderer.sprite = data.destroyedSprite;
		data.destroyEffect.Create(references.gameObject.transform);
		data.clickSound.Play(references.audioSource);
		UnityEngine.Object.Destroy(references.collider2d);

		references.onDestroy.Invoke(this);
	}

	public void OnDestroy(Action<Item> a) {
		references.onDestroy.OnDestroy += a;
	}
}

