﻿using System;
using System.Collections.Generic;

public class Room {

	private RoomReferences references;
	private Dictionary<Guid, Item> items;

	public Room(RoomReferences references) {
		this.references = references;
		this.items = new Dictionary<Guid, Item>();
	}

	public void AddItem(Item item) {
		items[item.guid] = item;
	}

	public void RemoveItem(Item item) {
		items.Remove(item.guid);

		if(items.Count == 0) {
			references.onComplete.Do();
			references.FadeOut();
		}
	}

	public void Destroy() {
		items.Clear();
		references.FadeOut();
	}
}

