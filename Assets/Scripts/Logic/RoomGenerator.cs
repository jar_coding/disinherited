﻿
using System;
using System.Collections.Generic;
using UnityEngine;

public class RoomGenerator {

	private const int WIDTH = 9;
	private const int HEIGHT = 7;

	public RoomGeneratorSettings settings;
	private PossibleItems possibleItemPrefabs;
	private bool[,] slots;

	public RoomGenerator(RoomGeneratorSettings settings) {
		this.settings = settings;

		if (settings == null) return;

		this.slots = new bool[WIDTH, HEIGHT];
		this.possibleItemPrefabs = new PossibleItems(settings.possibleItems);
	}

	public Room CreateTestRoom(Action onCompleteAction, Action<Item> onItemDestroyAction) {

		GameObject g = UnityEngine.Object.Instantiate(settings.roomPrefab);

		RoomReferences roomReference = g.GetComponent<RoomReferences>();
		roomReference.onComplete.DoAction += onCompleteAction;

		Room room = new Room(roomReference);

		foreach(ItemSpawnChance itemSpawnChance in settings.possibleItems) {

			ItemData itemData = itemSpawnChance.itemData;

			int w = itemData.Width;
			int h = itemData.Height;

			for (int x = 0; x < slots.GetLength(0); x += w) {
				for (int y = 0; y > -slots.GetLength(1); y -= h) {

					bool xOutOfBounds = x + w > slots.GetLength(0);
					bool yOutOfBounds = y - h < -slots.GetLength(1);

					if (!xOutOfBounds && !yOutOfBounds) {

						ItemReferences itemRef = itemData.Create(x, y, roomReference.itemsContainer.transform);
						Item item = new Item(itemRef, itemData);
						room.AddItem(item);
						item.OnDestroy(room.RemoveItem);
						item.OnDestroy(onItemDestroyAction);
					}
				}
			}
		}

		roomReference.FadeIn();

		return room;
	}


	public void Create() {
		for (int x = 0; x < slots.GetLength(0); x++) {
			for (int y = 0; y < slots.GetLength(1); y++) {

				if (IsBlocked(x, y))
					continue;

				if (IsWall(y)) {
					// only Pictures
				} else {
					
					if(IsBlocked(x, y - 1)) {
						if (IsBlocked(x + 1, y)) {
							// only items with h = 1 && w = 1
						} else {
							// items with h = 1 && w = n
						}
					} else {
						if(IsBlocked(x + 1, y)) {
							// only items with h = n && w = 1
						} else {
							// items with h = n && w = n
						}
					}
				} 
			}
		}
	}

	private bool IsBlocked(int x, int y) {
		if (x < 0 || x > slots.GetLength(0)) return true;
		if (y < 0 || y > slots.GetLength(1)) return true;

		return !slots[x, y];
	}

	private bool IsWall(int y) {
		return y == 0;
	}
}

public class PossibleItems {
	private readonly Dictionary<int, Dictionary<int, List<ItemData>>> items;

	public PossibleItems(ItemSpawnChance[] itemChances) {
		items = new Dictionary<int, Dictionary<int, List<ItemData>>>();

		foreach(ItemSpawnChance itemSpawnChance in itemChances) {

			ItemData itemData = itemSpawnChance.itemData;

			this[itemData.Width, itemData.Height].Add(itemData);
		}
	}

	public List<ItemData> this[int w, int h] {
		get {
			if (!items.ContainsKey(w))
				items.Add(w, new Dictionary<int, List<ItemData>>());

			if (!items[w].ContainsKey(h))
				items[w].Add(h, new List<ItemData>());

			return items[w][h];
		}
	}
}
