﻿using System;

public enum IntroInternalState {
	FADE_IN,
	TEXT_1,
	TEXT_2,
	FADE_OUT,
	NONE
}

public static class Extensions {
	public static IntroInternalState NextState(this IntroInternalState state) {
		switch(state) {
			case IntroInternalState.FADE_IN:
				return IntroInternalState.TEXT_1;
			case IntroInternalState.TEXT_1:
				return IntroInternalState.TEXT_2;
			case IntroInternalState.TEXT_2:
				return IntroInternalState.FADE_OUT;
		}
		return IntroInternalState.NONE;
	}
}
