﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class OutroState : GameState {

	private ClockReferences clockRef;
	private DialogReferences dialogRef;
	private bool complete;

	public OutroState(Game game) : base(game) {
		clockRef = game.references.uiReference.clockReferences;
		dialogRef = game.references.uiReference.dialogReferences;
		Start();
	}

	public void Start() {

		clockRef.settings.timeHasExpiredSound.Play(clockRef.audioSource);
		Ring(true);

		Sequence s = DOTween.Sequence();
		s.AppendInterval(1.2f).onComplete = () => {
			Ring(false);
			FadeIn();
			clockRef.FadeOut();
		};
		s.Play();
	}

	private void Ring(bool isRinging) {
		clockRef.animator.SetBool("isRinging", isRinging);
	}

	public override void Update() {

		if (!complete) return;

		if (Input.GetKeyDown(KeyCode.Space)) {
			game.references.uiReference.scoreReferences.ResetScore();
			FadeOut();
		}
	}

	private void FadeIn() {
		Text textRef = dialogRef.text;
		textRef.text = "";
		Sequence s = DOTween.Sequence();
		s.Append(dialogRef.transform.DOLocalMoveY(0, 0.5f).SetEase(Ease.InFlash));
		s.AppendInterval(1);
		s.Join(dialogRef.bubble.transform.DOScale(2, 0.5f));
		s.Play();
		s.onComplete = () => {
			complete = true;
			textRef.DOText("END VALUE: " + game.Score, 0.2f);
		};
	}

	private void FadeOut() {
		complete = false;
		dialogRef.transform.DOLocalMoveY(-500, 0.2f).SetEase(Ease.OutFlash).onComplete = () => {
			game.state = new PlayState(game);
			dialogRef.bubble.transform.DOScale(0, 0);
		};
	}
}
