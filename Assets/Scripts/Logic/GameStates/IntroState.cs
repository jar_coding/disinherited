﻿using DG.Tweening;
using UnityEngine;

public class IntroState : GameState {

	private IntroInternalState state;
	private DialogReferences dialogRef;

	public IntroState(Game game) : base(game) {
		state = IntroInternalState.FADE_IN;
		dialogRef = game.references.uiReference.dialogReferences;
		UpdateStates();
	}

	public override void Update() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			UpdateStates();
		}
	}

	private void UpdateStates() {
		switch (state) {
			case IntroInternalState.FADE_IN:
				FadeIn();
				break;
			case IntroInternalState.TEXT_1:
				dialogRef.text.DOText("Dialog Text1", 0.2f, false);
				break;
			case IntroInternalState.TEXT_2:
				dialogRef.text.text = "";
				dialogRef.text.DOText("Dialog Text2", 0.2f, false);
				dialogRef.face.sprite = dialogRef.happyFace;
				break;
			case IntroInternalState.FADE_OUT:
				FadeOut();
				break;
		}

		state = state.NextState();
	}

	private void FadeIn() {
		Sequence s = DOTween.Sequence();
		s.Append(dialogRef.transform.DOLocalMoveY(0, 0.5f).SetEase(Ease.InFlash));
		s.AppendInterval(1);
		s.Join(dialogRef.bubble.transform.DOScale(2, 0.5f));
		s.AppendInterval(0.5f);
		s.Play();
	}

	private void FadeOut() {
		Sequence s = DOTween.Sequence();
		s.Append(dialogRef.transform.DOLocalMoveY(-500, 0.2f).SetEase(Ease.OutFlash));
		s.onComplete = () => {
			//dialogRef.face.gameObject.SetActive(false);
			//dialogRef.bubble.gameObject.SetActive(false);
			game.state = new PlayState(game);
			dialogRef.bubble.transform.DOScale(0, 0);
		};
		s.Play();
	}
}
