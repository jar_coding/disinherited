﻿using UnityEngine;
using DG.Tweening;

public class PlayState : GameState {

	private bool gameOver;
	private float currentTime;
	private RoomGenerator roomGenerator;
	private Room currentRoom;

	private long score;

	public PlayState(Game game) : base(game) {
		roomGenerator = new RoomGenerator(game.settings.roomGeneratorSettings);
		CreateRoom();
		game.references.uiReference.clockReferences.FadeIn();
	}

	public override void Update() {
		currentTime += Time.deltaTime;

		if(currentTime >= game.settings.clockSettings.maximumDuration) {
			GameOver();
			return;
		}

		game.references.uiReference.clockReferences.Rotate(currentTime);
	}

	private void GameOver() {
		game.Score = score;
		game.state = new OutroState(game);
		currentRoom.Destroy();
		gameOver = true;
	}

	public void CreateRoom() {
		if (gameOver) return;
		currentRoom = roomGenerator.CreateTestRoom(RoomCompleted, IncreaseScore);
	}

	public void RoomCompleted() {
		Debug.Log("Room Completed");
		CreateRoom();
	}

	public void IncreaseScore(Item item) {
		score += item.data.value;
		game.references.uiReference.scoreReferences.SetScore(score);
		game.settings.textPopUpEffect.Create(game.references.uiReference.uiRectTransform, item);
	}
}
