﻿public abstract class GameState {

	protected readonly Game game;

	public GameState(Game game) {
		this.game = game;
	}

	public abstract void Update();
}