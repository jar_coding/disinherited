﻿using UnityEngine;

// Contains everything scene reference
public class References : MonoBehaviour {
	public UIReferences uiReference;
	public AudioSource backgroundAudioSource;
}
