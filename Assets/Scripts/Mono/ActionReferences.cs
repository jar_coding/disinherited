﻿using System;
using UnityEngine;

public class ActionReferences : MonoBehaviour
{
	public event Action DoAction;
	public void Do() {
		if (DoAction != null) {
			DoAction.Invoke();
		}
	}
}
