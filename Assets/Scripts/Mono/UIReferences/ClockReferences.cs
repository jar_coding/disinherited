﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ClockReferences : MonoBehaviour {

	public ClockSettings settings;

	public Image clock;
	public Image watchhand;
	public Image shadow;

	public Animator animator;

	public AudioSource audioSource;

	public void Rotate(float time) {
		watchhand.transform.localRotation = Quaternion.Euler(0, 0, -time * 360.0f / settings.maximumDuration + 90.0f);
	}

	public void FadeIn() {
		Fade(1, clock);
		Fade(1, watchhand);
		Fade(1, shadow);
	}

	public void FadeOut() {
		Fade(0, clock);
		Fade(0, watchhand);
		Fade(0, shadow);
	}

	private void Fade(float value, Image image) {
		image.DOFade(value, 0.3f);
	}
}
