﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIReferences : MonoBehaviour {
	public RectTransform uiRectTransform;
	public ScoreReferences scoreReferences;
	public ClockReferences clockReferences;
	public ButtonReferences buttonReferences;
	public DialogReferences dialogReferences;
}
