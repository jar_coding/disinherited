﻿using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;

public class ScoreReferences : MonoBehaviour {
	public Text scoreText;
	private long value;

	public void SetScore(long newValue) {
		DOTween.To(() => value, x => value = x, newValue, 0.5f);
	}

	public void ResetScore() {
		value = 0;
	}

	public void Update() {
		scoreText.text = value + " $";
	}
}

