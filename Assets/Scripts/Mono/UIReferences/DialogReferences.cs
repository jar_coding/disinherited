﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DialogReferences : MonoBehaviour {
	public Image face;
	public Image bubble;
	public Text text;

	public Sprite happyFace;
	public Sprite angryFace;
}
