﻿using System;
using UnityEngine;
using DG.Tweening;

public class ItemReferences : MonoBehaviour {

	public ItemData data;

	public SpriteRenderer spriteRenderer;
	public ActionReferences clickReference;
	public ItemDestroyedActionReferences onDestroy;
	public AudioSource audioSource;
	public BoxCollider2D collider2d;

	public void FadeOut() {
		spriteRenderer.DOFade(0f, 1f).onComplete = () => {
			Destroy(gameObject);
		};
	}

	public void Hover(bool enter) {
		data.wobbleEffect.Create(transform, enter);
	}
}


