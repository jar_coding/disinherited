﻿using System;
using UnityEngine;

public class ItemDestroyedActionReferences : MonoBehaviour {
	public event Action<Item> OnDestroy;

	public void Invoke(Item item) {
		if (OnDestroy != null) {
			OnDestroy.Invoke(item);
		}
	}
}
