﻿using UnityEngine;

public class GameController : MonoBehaviour {

	public GameSettings settings;
	public References references;

	private Game game;

	void Start () {
		game = new Game(references, settings);
	}
	
	void Update () {
		game.Update();
	}
}
