﻿using UnityEngine;
using DG.Tweening;

public class RoomReferences : MonoBehaviour {

	public RoomSettings settings;

	public GameObject itemsContainer;
	public SpriteRenderer spriteRenderer;
	public ActionReferences onComplete;

	public void FadeOut() {
		spriteRenderer.DOFade(0f, settings.roomFadeOutTime);
		transform.DOMoveX(15, settings.roomFadeOutTime).SetEase(settings.roomEaseOut).OnComplete(() => {
			Destroy(gameObject);
		});
	}

	public void FadeIn() {
		transform.position = new Vector3(-15, 0, 0);
		spriteRenderer.DOFade(1f, settings.roomFadeOutTime);
		transform.DOMoveX(0, settings.roomMoveInTime).SetEase(settings.roomEaseIn);
	}
}

