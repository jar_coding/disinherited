﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Effect/TextPopUp")]
public class TextPopUpEffect : ScriptableObject {

	public GameObject popUpPrefab;

	public float punchTime;
	public float showTime;
	public float fadeTime;

	public void Create(Transform transform, Item item) {
		GameObject gameObject = Instantiate(popUpPrefab, transform);
		gameObject.transform.position = Camera.main.WorldToScreenPoint(item.references.transform.position);

		UIPopupReferences reference = gameObject.GetComponent<UIPopupReferences>();

		reference.text.text = item.data.value.ToString();

		Sequence popupEffect = DOTween.Sequence();
		popupEffect.Append(gameObject.transform.DOMoveY(gameObject.transform.position.y + 50f, punchTime));
		popupEffect.Join(gameObject.transform.DOShakeScale(punchTime, 2));
		popupEffect.AppendInterval(showTime);
		popupEffect.Join(reference.text.DOFade(0.0f, fadeTime));
		popupEffect.OnComplete(() => Destroy(gameObject));
	}
}
