﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

[CreateAssetMenu(menuName = "Effect/DestroyEffect")]
public class DestroyEffect : Effect {

	public GameObject effectPrefab;
	public float delay;

	public override void Create(Transform transform) {
		Destroy(Instantiate(effectPrefab, transform), delay);
	}
}
