﻿using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(menuName = "Effect/WobbleEffect")]
public class WobbleEffect : ScriptableObject {

	public float duration;
	public float strength;

	public void Create(Transform transform, bool enter) {
		transform.DOScale(enter ? strength : 1, duration);
	}
}
