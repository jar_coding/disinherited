﻿using UnityEngine;
using System.Collections;

public abstract class Effect : ScriptableObject {
	public abstract void Create(Transform transform);
}
