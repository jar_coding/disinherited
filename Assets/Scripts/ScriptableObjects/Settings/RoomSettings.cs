﻿using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(menuName = "Settings/RoomSettings")]
public class RoomSettings : ScriptableObject
{
	public Ease roomEaseIn;
	public Ease roomEaseOut;

	public float roomMoveInTime;
	public float roomMoveOutTime;

	public float roomFadeOutTime;
}

