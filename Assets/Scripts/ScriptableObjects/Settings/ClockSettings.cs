﻿using UnityEngine;

[CreateAssetMenu(menuName = "Settings/ClockSettings")]
public class ClockSettings : ScriptableObject {
	public float maximumDuration;
	public SimpleAudioEvent timeHasExpiredSound;
}
