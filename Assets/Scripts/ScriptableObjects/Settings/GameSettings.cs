﻿using DG.Tweening;
using UnityEngine;

[CreateAssetMenu(menuName = "Settings/GameSettings")]
public class GameSettings : ScriptableObject {
	public RoomGeneratorSettings roomGeneratorSettings;
	public RoomSettings roomSettings;
	public ClockSettings clockSettings;
	public TextPopUpEffect textPopUpEffect;
	public SimpleAudioEvent backgroundMusic;
}