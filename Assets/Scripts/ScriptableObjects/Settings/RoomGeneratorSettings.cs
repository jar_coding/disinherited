﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Settings/RoomGeneratorSettings")]
public class RoomGeneratorSettings : ScriptableObject {
	public GameObject roomPrefab;
	public Sprite[] backgrounds;
	
	public ItemSpawnChance[] possibleItems;
}

