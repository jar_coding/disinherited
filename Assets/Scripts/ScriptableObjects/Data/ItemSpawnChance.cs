﻿
using UnityEngine;

[CreateAssetMenu]
public class ItemSpawnChance : ScriptableObject {
	public float chance;
	public ItemData itemData;
}