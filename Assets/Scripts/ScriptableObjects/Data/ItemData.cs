﻿using UnityEngine;

[CreateAssetMenu]
public class ItemData : ScriptableObject {
	public GameObject prefab;

	public DestroyEffect destroyEffect;
	public WobbleEffect wobbleEffect;
	public SimpleAudioEvent clickSound;

	public Sprite sprite;
	public Sprite destroyedSprite;

	public int value;
	public bool attachableToTheWall;

	public int Width {
		get { return GetRatio(sprite.texture.width); }
	}
	public int Height {
		get { return GetRatio(sprite.texture.height); }
	}

	private int GetRatio(float value) {
		if (sprite == null) return 0;

		return (int) (value / 16);
	}

	public ItemReferences Create(int x, int y, Transform parent) {
		GameObject go = Instantiate(
			prefab,
			new Vector3(x + Width / 2f, y - Height / 2f, 0),
			new Quaternion(),
			parent);

		return go.GetComponent<ItemReferences>();
	}
}

